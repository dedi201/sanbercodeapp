import React, { useState } from 'react';
import {
  Button,
  FlatList,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import useInputValue from '../src/components/hooks/form';
import useTodos from '../src/components/hooks/todos';
const Layout = (props) => (
  <View style={{flex: 1}}>

      <StatusBar barStyle="light-content" backgroundColor="darkblue" />
    <View
      style={{backgroundColor: 'darkblue', alignItems: 'center', padding: 10}}>
      <Text style={{fontSize: 25, color: 'white'}}>Todo Apps Dedi</Text>
    </View>
    {props.children}
  </View>
);

const FormTodo = ({inputValue,changeInput,clearInput,addTodo}) => (
  <View style={{flexDirection: 'row'}}>
  <View style={{flex:1}} >
    <TextInput
      placeholder="Add New Todo"
      style={{
        flex: 1,
        fontSize: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'blue',
        paddingLeft: 10,
      }}
      onChangeText={text => changeInput(text)}
      value={inputValue}
    />
    </View>
    <View style={{width:50}}>
    <Button  onPress={() => {
        addTodo(inputValue);
        clearInput();
    }}
     title={'+'} />
  </View>
  </View>
);

//component list

const TodoItem = ({item, onPressItem, removeTodo}) => (
  <View
    style={{flexDirection: 'row', alignContent: 'space-between', padding: 5}}>
    <TouchableOpacity onPress={() => onPressItem(item.id)} style={{flex: 1}}>
      <Text
        style={{
          fontSize: 18,
          textDecorationLine: item.completed ? 'line-through' : 'none',
        }}>
        {item.text}
      </Text>
    </TouchableOpacity>

    <TouchableOpacity onPress={() => removeTodo(item.id)}>
      <FontAwesome5 name="trash-alt" size={20} />
    </TouchableOpacity>
  </View>
);

const List = ({todos, toggleTodo, removeTodo}) => (
  <FlatList
    data={todos}
    renderItem={({item}) => (
      <TodoItem item={item} onPressItem={toggleTodo} removeTodo={removeTodo} />
    )}
    keyExtractor={(item, index) => item.id.toString()}
    ListEmptyComponent={() => (
      <View style={{flex: 1, alignItems: 'center', marginTop: 20}}>
        <Text style={{fontSize: 25}}>TODO IS EMPTY</Text>
      </View>
    )}
    ItemSeparatorComponent={() => (
      <View style={{height: 0.3, backgroundColor: 'rgba(0,0,0,0.4)'}} />
    )}
  />
);

const TodoList = () => {
  const {todos, toggleTodo, removeTodo, addTodo} = useTodos();
  const {inputValue, changeInput, clearInput} = useInputValue();
  return (
    <Layout>
      <FormTodo
        inputValue={inputValue}
        changeInput={changeInput}
        clearInput={clearInput}
        addTodo={addTodo}
      />
      <List todos={todos} toggleTodo={toggleTodo} removeTodo={removeTodo} />
    </Layout>
  );
};

export default TodoList;

const styles = StyleSheet.create({});
