import React from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {IMProfile} from '../src/assets';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

function HomeScreen() {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" backgroundColor="#f4511e" />
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 1,
          padding: 20,
          backgroundColor: 'white',
        }}>
        <Image source={IMProfile} style={styles.gambar} />
        <Text style={styles.text}>Dedi Irawan</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 5,
          backgroundColor: 'white',
          padding: 20,
        }}>
        <FontAwesome5 name="wallet" size={30} color="#900" />
        <Text style={{marginLeft: -150, textAlignVertical: 'center'}}>
          Saldo
        </Text>
        <Text style={{textAlignVertical: 'center'}}>Rp. 120.000.000</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          padding: 20,
          borderBottomWidth: 1,
          borderBottomColor:'#CECECE',
          backgroundColor: 'white',
        }}>
        <FontAwesome5 name="cogs" size={30} color="#900" />
        <Text style={{marginLeft: 20, textAlignVertical: 'center'}}>
          Pengaturan
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          borderBottomWidth: 1,
          borderBottomColor:'#CECECE',
          padding: 20,
          backgroundColor: 'white',
        }}>
        <FontAwesome5 name="question-circle" size={30} color="#900" />
        <Text style={{marginLeft: 25, textAlignVertical: 'center'}}>
          Bantuan
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 5,
          padding: 20,
          backgroundColor: 'white',
        }}>
        <FontAwesome5 name="fan" size={30} color="#900" />
        <Text style={{marginLeft: 25, textAlignVertical: 'center'}}>
          Syarat & Ketentuan
        </Text>
      </View>
      <TouchableOpacity
        style={{flexDirection: 'row', padding: 20, backgroundColor: 'white'}}>
        <FontAwesome5 name="sign-out-alt" size={30} color="#900" />
        <Text style={{marginLeft: 25, textAlignVertical: 'center'}}>
          Bantuan
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createStackNavigator();

const TugasDua = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Account',
            headerStyle: {
              backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default TugasDua;

const styles = StyleSheet.create({
  text: {color: 'black', padding: 10, fontWeight: 'bold', fontSize: 16},
  wrapper: {backgroundColor: '#CECECE', flex: 1},
  gambar: {width: 50, height: 50, marginRight: 20},
});
