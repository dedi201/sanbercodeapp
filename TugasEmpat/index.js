import React, { createContext } from 'react';
import React from 'react'
import { View, Text } from 'react-native'

export const RootContext = createContext();

const Contex = () => {
    return (
        <RootContext.Provider value={{
            input,
            todos
        }}>
            <TodoList />
        </RootContext.Provider>
    )
}


export default Contex;
