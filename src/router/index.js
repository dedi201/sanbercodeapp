import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React, {useEffect, useState} from 'react';
import {Text} from 'react-native';
import { Button } from '../components';
import {Splashscreens, Intro, Login, Profile} from '../pages';

const Stack = createStackNavigator();

const MainNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{
        title: 'Masuk', headerStyle:{
            backgroundColor:'skyblue'
        }
      }}
    />
     <Stack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerLeft: () => null,
          title:'Account',
          headerStyle:{
            backgroundColor:'#f4511e',}
        }}/>
  </Stack.Navigator>
);

const AppNavigation = () => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 3000);
  }, []);

  if (isLoading) {
    return <Splashscreens />;
  }

  return (
    <NavigationContainer>
      <MainNavigation />
    </NavigationContainer>
  );
};

export default AppNavigation;
