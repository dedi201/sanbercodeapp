import React, {useEffect} from 'react';
import AppNavigation from './router';
import firebase from '@react-native-firebase/app'
// import * as firebase from "firebase"

var firebaseConfig = {
  apiKey: "AIzaSyBVc3OC9GUVCaKklyrfGS05VM52zM-xQbE",
  authDomain: "sanbercode-4e056.firebaseapp.com",
  projectId: "sanbercode-4e056",
  storageBucket: "sanbercode-4e056.appspot.com",
  messagingSenderId: "537657866229",
  appId: "1:537657866229:web:985cc4acb5a299096752bc",
  measurementId: "G-25023BXFNW"
};
// Initialize Firebase
if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}


const App: () => React$Node = () => {

  return (
    <AppNavigation />
  );
};

export default App;

