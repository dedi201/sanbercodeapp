import React, {useState} from 'react';
import {StyleSheet, Text, View,TextInput} from 'react-native';
import {ScrollView, } from 'react-native-gesture-handler';

const Input = ({placeBox, secureTextEntry,}) => {
  const [border, setBorder] = useState('#E9E9E9');
  const onFocusForm = () => {
    setBorder('#124A84');
  };
  const onBlurForm = () => {
    setBorder('#E9E9E9');
  };
  return (
    <View>
        <TextInput
          onFocus={onFocusForm}
          onBlur={onBlurForm}
          placeholder={placeBox}
          style={styles.input(border)}
              secureTextEntry={secureTextEntry}
             />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  input: (border) => ({
    borderWidth: 1,
    borderColor: border,
    padding: 12,
  }),
});
