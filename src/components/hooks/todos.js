import React, { useState } from 'react'
import { View, Text } from 'react-native'

let mytodos = [
    {
      id: 1,
      text: 'Belajar React Native di sanbercode',
      completed: false,
    },
    {
      id: 2,
      text: 'Belajar React Hooks di sanbercode',
      completed: false,
    },
  ];

const useTodos = (initialValue=mytodos) => {
    const [ todos, setTodos] = useState(initialValue);
    return {
        todos,
        addTodo: (text) => {
            setTodos([
                ...todos,
                {
                    id: new Date().getTime(),
                    text,
                    completed:false
                }
            ])
        },
        toggleTodo: (id) => {
            setTodos(
            todos.map(todo => 
                todo.id === id ? {...todo, completed: !todo.completed} : todo
                )
            );
    },
        removeTodo: id => setTodos (todos.filter(todo =>todo.id !== id))
    };
};

export default useTodos;
