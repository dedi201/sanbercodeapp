import React, { useState } from 'react'
import { View, Text } from 'react-native'

const useInputValue = (initialValue = '') => {
    const [inputValue, setInputValue] = useState(initialValue);
    return {
        inputValue,
        changeInput: text => setInputValue(text),
        clearInput: () => setInputValue("")
    };
};

export default useInputValue;
