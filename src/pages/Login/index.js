import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, TextInput, View} from 'react-native';
import {Button,Gap, Input, Link} from '../../components';
import uri from '../../api';
import auth from '@react-native-firebase/auth'
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage';
import TouchID from 'react-native-touch-id';
import RNFetchBlob from 'rn-fetch-blob';

const config = {
  title : 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Failed',
  cancelText: 'Cancel'
}

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // const saveToken = async (token) => {
  //   try {
  //     await AsyncStorage.setItem('token', token);
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  useEffect(() => {
    configureGoogleSignIn()
  }, []);
  
  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:'537657866229-d6jv45ted49tp031b1gbnb66h41k25ga.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('signInWithGoogle -> idToken', idToken);

      const credential = auth.GoogleAuthProvider.credential(idToken)

      auth().signInWithCredential(credential)

      navigation.navigate('Profile')                            
    } catch (error) {
      console.log('signInWithGoogle -> error', error);
    }
  };

  const signInWithFingerPrint = () => {
    TouchID.authenticate('', config)
    .then(success => {
      alert("Authentication success")
    })
    .catch(err => {
      alert("Authentication error")
    })
  }

//config auth jwt

const onPasswordPress = () => {
  setVisible(false)
  setInput(true)
}

const onEMailPress = () => {
  setVisible(false)
  navigation.navigate('Verification', item)
}

const saveToken = async (item) => {
  try{
    if(item !== null){
      await AsyncStorage.setItem("token", item.data.token)
    }
   } catch(err){
      console.log(err);
    }
  }


  const onLoginPress = () => {
    let body = {
      email: email,
      password: password,
    };
    RNFetchBlob.config({
      trusty:true
    })
    .fetch('POST', `${uri.api}/auth/login`,{
      'Content-Type': 'application/json'
    }, JSON.stringify(body))
    .then((response) => {
      const data = JSON.parse(response.data)
      saveToken(data)
      navigation.navigate('Profile')
    })
    .catch((err) => {
      console.log("Login -> err", err);
  })
  };

  return (
    <View style={styles.container}>
      <Text style={{textAlign: 'center', marginBottom: 10}}>
        Masukan Email dan Password
      </Text>
      <Gap height={10} />
      <TextInput
        placeholder="masukan nama"
        underlineColorAndroid="#c6c6c6"
        value={email}
        onChangeText={(email) => setEmail(email)}
      />
      <TextInput
        placeholder="password"
        value={password}
        underlineColorAndroid="#c6c6c6"
        onChangeText={(password) => setPassword(password)}
        secureTextEntry
      />
      <View style={{marginTop: 10}}>
        <Button title="Continue" onPress={() => onLoginPress()} />
      </View>
      <View style={{marginTop: 10}}>
        <GoogleSigninButton
          onPress={() => signInWithGoogle()}
          style={{width: '100%', marginBottom: 10}}
          size={GoogleSigninButton.Size.Wide}
        />
        <View>
          <Button title="Fingerprint" style={{flex: 1}} onPress={() => signInWithFingerPrint()} />
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 10,
          justifyContent: 'center',
        }}>
        <Text style={{}}>Belum punya akun? </Text>
        <Link title="Daftar" />
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', padding: 20},
});
