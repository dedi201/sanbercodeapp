import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button, Input, Link } from '../../components';

const KodeVerfikasi = () => {
    return (
        <View style={styles.container}>
      <View style={styles.contentContainer}>
        <Text style={styles.title}>Perjalanan kebaikanmu dimulai disini</Text>
        <Text>Masukan 6 digit kode yang kami kirim ke real.zakkymf@gmail.com</Text>
      </View>
      <View style={styles.contentContainer}>
        <Input label="Masukan kode" />
      </View>
      <View>
        <Button title="Daftar" />
      </View>
      <View style={styles.containerRegister}>
        <Text >Sudah punya akun?   
        </Text>
          <Link title=" Masuk"/>
      </View>
    </View>
  );
};

export default KodeVerfikasi;

const styles = StyleSheet.create({
  container:{padding:10, flex:1},
  contentContainer:{marginTop:10, marginBottom:10 },
  title:{textAlign:'center',fontWeight:'bold', fontSize:16},
  containerRegister:{alignItems:'center', flexDirection:'row', justifyContent:'center'}
});
    

