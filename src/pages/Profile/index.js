import React, { useEffect, useState } from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {IMProfile} from '../../assets';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Button,Gap } from '../../components';
import Axios from 'axios';
import uri from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin } from '@react-native-community/google-signin';

function Profile({navigation}) {
  const[userInfo,setUserInfo] = useState(null)

  useEffect(() => {
    async function getToken(){
      try {
        const token = await AsyncStorage.getItem('token')
        return getVenue(token)
        console.log("getToken -> token", token)
      } catch(err){
        console.log(err)
      }
    }     
    getToken()
    getCurrentUser()
    },[userInfo])

    const getCurrentUser = async () => {
      const userInfo = await GoogleSignin.signInSilently()
      console.log("getCurrentUser -> userInfo", userInfo)
      setUserInfo(userInfo)
    }
    
    const onLogoutPress = async () => {
      try {
        await GoogleSignin.revokeAccess()
        await GoogleSignin.signOut()
        await AsyncStorage.removeItem("token")
        navigation.navigate('Login')
      } catch(err){
        console.log(err)
      }
    }
  const getVenue = (token) => {
    Axios.get(`${uri}/vanues`, {
      timeout: 2000,
      headers:{
        'Authorization' : 'Bearer' + token
      }
    })
    .then((res) => {
      console.log("Profile -> res", res);
    })
    .catch((err) => {
      console.log("Profile -> err", err);
    })
  }


  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" backgroundColor="#f4511e" />
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 1,
          padding: 20,
          backgroundColor: 'white',
        }}>
        <Image source={IMProfile} style={styles.gambar} />
        <Text style={styles.text}>{userInfo && userInfo.user && userInfo.user.name}</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 5,
          backgroundColor: 'white',
          padding: 20,
        }}>
        <FontAwesome5 name="wallet" size={30} color="#900" />
        <Text style={{marginLeft: -150, textAlignVertical: 'center'}}>
          Saldo
        </Text>
        <Text style={{textAlignVertical: 'center'}}>Rp. 120.000.000</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          padding: 20,
          borderBottomWidth: 1,
          borderBottomColor:'#CECECE',
          backgroundColor: 'white',
        }}>
        <FontAwesome5 name="cogs" size={30} color="#900" />
        <Text style={{marginLeft: 20, textAlignVertical: 'center'}}>
          Pengaturan
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          borderBottomWidth: 1,
          borderBottomColor:'#CECECE',
          padding: 20,
          backgroundColor: 'white',
        }}>
        <FontAwesome5 name="question-circle" size={30} color="#900" />
        <Text style={{marginLeft: 25, textAlignVertical: 'center'}}>
          Bantuan
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 5,
          padding: 20,
          backgroundColor: 'white',
        }}>
        <FontAwesome5 name="fan" size={30} color="#900" />
        <Text style={{marginLeft: 25, textAlignVertical: 'center'}}>
          Syarat dan Ketentuan
        </Text>
      </View>
      <TouchableOpacity
        style={{flexDirection: 'row', padding: 20, backgroundColor: 'white'}}>
        <FontAwesome5 name="sign-out-alt" size={30} color="#900" />
        <Text style={{marginLeft: 25, textAlignVertical: 'center'}}>
          Bantuan
        </Text>
      </TouchableOpacity>
<Gap height={10}/>
      <Button title="Logout" onPress={() => onLogoutPress()}/>
    </View>
  );
}



export default Profile;

const styles = StyleSheet.create({
  text: {color: 'black', padding: 10, fontWeight: 'bold', fontSize: 16},
  wrapper: {backgroundColor: '#CECECE', flex: 1},
  gambar: {width: 50, height: 50, marginRight: 20},
});
