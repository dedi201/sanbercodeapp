import React, { useEffect, useState } from 'react'
import { useRef } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image,  } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { RNCamera } from 'react-native-camera';
import { Icon } from 'react-native-vector-icons/icon';
import { colors } from '../../utils/colors';
import { TextInput } from 'react-native-gesture-handler';
import { color } from 'react-native-reanimated';
import { Button } from '../../components';
const editAccount = ({navigation, route}) => {
    let input = useRef(null)
    let camera = useRef(null)
    const [editable, setEditable] = useState(false)
    const [token, setToken] = useState('')
    const [name, setName] = useState(route.params.name)
    const [email, setEmail] = useState(route.params.email)
    const [isVisible, setIsVisible] = useState(false)
    const [type,setType] = useState('back')
    const [photo,setPhoto] = useState(null)

    useEffect(() => {
        const getToken = async() => {
            try{
                const token = await AsyncStorage.getItem('token')
                if(token !== null){
                    setToken(token)
                }
            } catch(err){
                console.log(err);
            }
        }
        getToken()
    }, [])

    const editData = () => {
        setEditable(editable)
    }

    const renderCamera = () => {
        return(
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{flex:1}}>
                    <RNCamera
                    style={{flex:1}} type={type} ref={camera}
                    >
                    <View style={styles.camFlipContainer}>
                        <TouchableOpacity style={styles.btnFlip}>
                            <Icon name="rotate-3d-variant" size={15}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.round} />
                    <View style={styles.rectangle}/>
                    <View style={styles.btnTakeContainer}>
                        <TouchableOpacity style={styles.btnTake}>
                        <Icon name="camera" size={30}/>
                        </TouchableOpacity>
                    </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }
    return (
        <View style={[styles.container, {backgroundColor: colors.white}]}>
            <View style={styles.profileContainer}>
                <Image source={route.params.photo !== null && photo == null} />
                <TouchableOpacity style={styles.rounded} activeOpacity={0.7}>
                    <Icon name="camera" size={15} color={colors.white}/>
                </TouchableOpacity>
            </View>
            <View style={styles.detailUser}>
                <View style={styles.editContainer}>
                    <View>
                        <Text style={styles.editTitle}>Nama Lengkap</Text>
                        <View style={styles.editItem}>
                            <TextInput
                            ref={input}
                            value={name}
                            editable={editable}
                            style={styles.input}
                            onChangeText={(value) => setName(value)}
                            />
                            <Icon name="edit-2" size={20} color={color.white} />
                        </View>
                    </View>
                </View>
                <Text style={styles.editTitle}>Email</Text>
                <View style={styles.editItem}>
                    <TextInput
                    value={email}
                    editable={false}
                    style={styles.input}
                    onChangeText={(value) => setEmail(value)}
                    />
                </View>
            </View>
            <View style={{marginTop: 30}}>
                <Button style={styles.saveBtn} activeOpacity={0.7}>
                    <Text style={styles.btnSaveText}>Simpan</Text>
                </Button>
            </View>
            {renderCamera()}
        </View>
            

    )
}

export default editAccount;

const styles = StyleSheet.create({

})
