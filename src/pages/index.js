import Splashscreens from './SplashScreen';
import Intro from './Onboarding';
import Login from './Login';
import Profile from './Profile';

export {
    Splashscreens,
    Intro,
    Login,
    Profile,
}