import AsyncStorage from '@react-native-community/async-storage';
import React from 'react';
import { useState } from 'react';
import {View, Text, StyleSheet,} from 'react-native';
import { Button, Input, Link } from '../../components';
import uri from '../../api/ApiLogin'
import RNFetchBlob from 'rn-fetch-blob';



const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');

  const saveToken = async (item) => {
    try{
      if(item !== null) {
        await AsyncStorage.setItem("token",item.data.token)
      }
    }catch (err){
      console.log(err);
    }
  }

  const onLoginPress = () => {
    let body ={
      email:email,
      password:password
    }
    RNFetchBlob.config({
      trusty:true
    })
    .fetch('POST',`${uri.api}/auth/login`, {
      'Content-type': 'application/json'
    }, JSON.stringify(body))
    .then((response) => {
      const data = JSON.parse(response.data)
      saveToken(data)
      navigation.navigate('KodeVerifikasi')
    })
    .catch((err) => {
      console.log("Login -> err", err);
    }) 
  }

  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
        <Text style={styles.title}>Dedi Irawan Register</Text>
      </View>
      <View style={styles.contentContainer}>
        <Input label="Nomer Ponsel dan Email" />
        <Input label="Nama Lengkap"/>
      </View>
      <View>
        <Button title="Daftar" onPress={() => onLoginPress()}/>
      </View>
      <View style={styles.containerRegister}>
        <Text >Sudah punya akun?   
        </Text>
          <Link title=" Masuk"/>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container:{padding:10, flex:1},
  contentContainer:{marginTop:10, marginBottom:10 },
  title:{textAlign:'center',fontWeight:'bold', fontSize:16},
  containerRegister:{alignItems:'center', flexDirection:'row', justifyContent:'center'}
});
