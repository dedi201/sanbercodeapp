import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
} from 'react-native';
// import colors from '../../style/colors'
// import { Button } from '../../components/Button'
//import module  react native app intro slider
import AppIntroSlider from 'react-native-app-intro-slider';
import {Button} from '../../components';

// data yang akan kita tampilkan sebagai onboarding aplikasi
const data = [
  {
    id: 1,
    image: require('../../assets/img/ob_1.png'),
    description: 'Pilih teman terbaikmu',
  },
  {
    id: 2,
    image: require('../../assets/img/ob_2.png'),
    description: 'Jadikan temenmu sebagai jalan menuju suksesmu',
  },
  {
    id: 3,
    image: require('../../assets/img/ob_3.png'),
    description: 'Sukses akan kamu raih',
  },
];

const Intro = ({navigation}) => {
  //tampilan onboarding yang ditampilkan dalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{}}>
          <Image
            source={item.image}
            style={{width: 300, height: 300}}
            resizeMethod="auto"
            resizeMode="contain"
          />
        </View>
        <Text style={styles.textList}>{item.description}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView
      style={{
        padding: 20,
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'white',
      }}>
      <View style={{flex: 1, justifyContent: 'space-evenly'}}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View style={{marginTop: 30}}>
          <Text style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold',color:'grey'}}>
            CrowdFunding
          </Text>
        </View>
        <View style={{flex: 1}}>
          {/* contoh menggunakan component react native app intro slider */}
          <AppIntroSlider
            data={data} //masukan data yang akan ditampilkan menjadi onBoarding, dia bernilai array
            renderItem={renderItem} // untuk menampilkan onBoarding dar data array
            renderNextButton={() => null}
            renderDoneButton={() => null}
            activeDotStyle={{backgroundColor: 'red'}}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
        <View style={{}}>
          <Button title="MASUK" onPress={() => navigation.navigate('Login')}/>
          <Button title="DAFTAR" onPress={() => navigation.navigate('Register')}/>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({

});

export default Intro;
